[[_TOC_]]

## Disclaimer

_todo_

## Supported tags

- `2.4-ubuntu`, `2.4`, `ubuntu`, `latest`
- `2.4-alpine3.15`, `2.4-alpine`, `alpine3.15`, `alpine`
- `2.4-alpine3.14`, `alpine3.14`

## How to use this image

_todo_

## Environment Variables

### `SVN_RAW_CONFIG`

Overrides all other config options. \
Default: `unset`

### `SVN_REPO_PATH`

Default: `"svn"`

### `SVN_REPOS_NAME`

Human-readable repository name. \
Default: `unset`

### `SVN_AUTH_TYPE`

Possible values: `"None"`, `"Basic"`, `"Digest"`, `"Form"`. \
Default: `unset`

### `SVN_AUTH_NAME`

Default: `unset`

### `SVN_AUTH_BASIC_PROVIDER`

Default: `unset`

### `SVN_AUTH_USER_FILE`

Default: `unset`

### `SVN_AUTH_LDAP_URL`

Default: `unset`

### `SVN_AUTH_LDAP_BIND_DN`

Default: `unset`

### `SVN_AUTH_LDAP_BIND_PASSWORD`

Default: `unset`

### `SVN_LIST_PARENT_PATH`

Default: `unset`

### `SVN_PATH_AUTHZ`

Default: `unset`

### `SVN_ADDITIONAL_CONFIG`

Additional options for configuration file. \
Default: `unset`

### `SVN_SKIP_CREATE_REPO`

Skip automatic repo creation. \
Default: `unset`

## Known problems

No known issues.

## License

The repository is licensed under `AGPL-3.0-or-later`. \
Container images contains code under various licenses.
